﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Alien_Run
{
    class Tile
    {

        //---------------------------------------------
        //Enums
        //---------------------------------------------

        public enum TileType
        {
            IMPASSIBLE, // = 0, blocks player movement from any direction
            PLATFORM, // = 1, blocks player movement downwards
            LETHAL, // = 2, kills upon contact
            GOAL, // = 3, you win if you touch it
            POINTS // = 4 Adds points upon being touched
        }


        //---------------------------------------------
        //Data
        //---------------------------------------------

        Texture2D sprite;
        Vector2 position;
        TileType type;
        int points = 0;
        bool visible = true;

        //---------------------------------------------
        //Behaviour
        //---------------------------------------------

        public Tile(Texture2D newTexture, Vector2 newPosition, TileType newType, int newPoints = 0)
        {
            sprite = newTexture;
            position = newPosition;
            type = newType;
            points = newPoints;
            
        }

        //---------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if(visible == true)
            spriteBatch.Draw(sprite, position, Color.White);
        }
        //---------------------------------------------
        public Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }
        //---------------------------------------------

        public TileType GetTileType()
        {
            return type;
        }
        //---------------------------------------------
        public int GetPoints()
        {
            return points;
        }
        //---------------------------------------------
        public void Hide()
        {
            visible = false;
        }

        //---------------------------------------------
        public bool GetVisible()
        {
            return visible;
        }


    }
}
