﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;

namespace Alien_Run
{
    class Level
    {

        //---------------------------------------------
        //Data
        //---------------------------------------------

        Tile[,] tiles;

        private Texture2D boxTexture;
        private Texture2D bridgeTexture;
        private Texture2D spikeTexture;
        private Texture2D flagTexture;
        private Texture2D bronzeCoinTexture;
        private Texture2D silverCoinTexture;
        private Texture2D goldCoinTexture;

        bool completedLevel = false;

        const int LEVEL_WIDTH = 100;
        const int LEVEL_HEIGHT = 100;

        const int TILE_WIDTH = 70;
        const int TILE_HEIGHT = 70;

        //---------------------------------------------
        //Behaviour
        //---------------------------------------------

        public void LoadContent(ContentManager content)
        {
            //creating texture that will eb used for all the tiles
            boxTexture = content.Load<Texture2D>("tiles/box");
            bridgeTexture = content.Load<Texture2D>("tiles/bridge");
            spikeTexture = content.Load<Texture2D>("items/spikes");
            flagTexture = content.Load<Texture2D>("tiles/goal");
            bronzeCoinTexture = content.Load<Texture2D>("items/coinBronze");
            silverCoinTexture = content.Load<Texture2D>("items/coinSilver");
            goldCoinTexture = content.Load<Texture2D>("items/coinGold");

            SetupLevel();
        }
        public void SetupLevel()
        {
            completedLevel = false;

            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            CreateBox(0, 9);
            CreateBox(1, 9);
            CreateBox(2, 9);
            CreateBox(3, 9);
            CreateBox(4, 9);
            CreateBox(5, 9);
            CreateBox(6, 9);
            CreateBox(7, 9);
            CreateBox(8, 9);

            // create platforms

            CreatePlatform(0, 5);
            CreatePlatform(1, 5);
            CreatePlatform(2, 5);
            CreatePlatform(3, 5);
            CreatePlatform(4, 5);
            CreatePlatform(9, 6);
            CreatePlatform(10, 6);
            CreatePlatform(11, 6);
            CreatePlatform(12, 6);

            //create spikes

            CreateSpikes(8, 8);

            //create flag

            CreateFlag(7, 8);

            //create coins

            CreateBronzeCoin(4, 6);
            CreateSilverCoin(5, 6);
            CreateGoldCoin(6, 6);
        }

        //---------------------------------------------
        public void CreateBox(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(boxTexture, tilePosition, Tile.TileType.IMPASSIBLE);
            tiles[tileX, tileY] = newTile;
        }

        public void CreatePlatform(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(bridgeTexture, tilePosition, Tile.TileType.PLATFORM);
            tiles[tileX, tileY] = newTile;
        }
        public void CreateSpikes(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(spikeTexture, tilePosition, Tile.TileType.LETHAL);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateFlag(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(flagTexture, tilePosition, Tile.TileType.GOAL);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateBronzeCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(bronzeCoinTexture, tilePosition, Tile.TileType.POINTS, 10);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateSilverCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(silverCoinTexture, tilePosition, Tile.TileType.POINTS, 25);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateGoldCoin(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(goldCoinTexture, tilePosition, Tile.TileType.POINTS, 50);
            tiles[tileX, tileY] = newTile;
        }
        //---------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; x++)
            {
                for (int y = 0; y < LEVEL_HEIGHT; y++)
                {
                    if (tiles[x, y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
        }

        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            //creating an empty list to fill with tiles 
            List<Tile> tilesInBounds = new List<Tile>();

            //determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / TILE_WIDTH) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / TILE_HEIGHT);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / TILE_HEIGHT) - 1;

            //loop through this range and add tiles to the list
            for (int x = leftTile; x <= rightTile; x++)
            {
                for (int y = topTile; y <= bottomTile; y++)
                {

                    //only if tile exists
                    //and if it is visible
                    Tile thisTile = GetTile(x, y);

                    if (GetTile(x, y) != null && thisTile.GetVisible() == true)
                        tilesInBounds.Add(tiles[x, y]);
                }
            }

            return tilesInBounds;
        }
        //---------------------------------------------
        public Tile GetTile(int x, int y)
        {
            if (x < 0 || x >= LEVEL_WIDTH || y < 0 || y >= LEVEL_HEIGHT)
                return null;

            return tiles[x, y];
        }
        //---------------------------------------------

        public void CompleteLevel()
        {
            completedLevel = true;
        }

        public bool GetCompletedLevel()
        {
            return completedLevel;
        }
    }
}
