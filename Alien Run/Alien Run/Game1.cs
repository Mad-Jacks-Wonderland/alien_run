﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Alien_Run
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteBatch UISpriteBatch;

        SpriteFont winFont;
        SpriteFont scoreFont;

        Player player = null;
        Camera camera = null;
        Level level = null;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            level = new Level();

            player = new Player(level);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            UISpriteBatch = new SpriteBatch(GraphicsDevice);

            //set screen size
            graphics.PreferredBackBufferWidth = GraphicsDevice.DisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsDevice.DisplayMode.Height;
            graphics.ApplyChanges();


            player.LoadContent(Content);
            level.LoadContent(Content);
            camera = new Camera(player, new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight));

            winFont = Content.Load<SpriteFont>("fonts/largeFont");
            scoreFont = Content.Load<SpriteFont>("fonts/mainFont");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            player.Update(gameTime);
            camera.Update();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);


            //main game Draw
            camera.Begin(spriteBatch);

            // TODO: Add your drawing code here

            player.Draw(spriteBatch);

            level.Draw(spriteBatch);

            spriteBatch.End();

            //UI draw
            UISpriteBatch.Begin();

            if(level.GetCompletedLevel() == true)
            {
                Vector2 textSize = winFont.MeasureString("You Win!");
                Vector2 screenCentre = new Vector2(GraphicsDevice.DisplayMode.Width / 2, GraphicsDevice.DisplayMode.Height / 2);
                UISpriteBatch.DrawString(winFont, "You win!", screenCentre - 0.5f*textSize, Color.White);
            }

            UISpriteBatch.DrawString(scoreFont, "SCORE: " + player.GetScore(), new Vector2(10, 10), Color.White);

            UISpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
