﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework.Input;

namespace Alien_Run
{
    class Player
    {

        //---------------------------------------------
        //Data
        //---------------------------------------------

        Vector2 position = Vector2.Zero;
        Vector2 prevPosition = Vector2.Zero;
        Texture2D sprite = null;
        Vector2 velocity = Vector2.Zero;
        private Level ourLevel = null;
        private bool touchingGround = false;
        private float jumpButtonTime = 0f;
        private bool jumpLaunchInProgress = false;
        private int score = 0;

        //constants
        const float MOVE_SPEED = 300.0f;
        const float GRAVITY_ACCEL = 3400.0f;
        const float TERMINAL_VEL = 550.0f;
        const float JUMP_LAUNCH_VEL = -2000.0f;
        const float MAX_JUMP_TIME = 1f;
        const float JUMP_CONTROL_POWER = 1f;

        //---------------------------------------------
        //Behaviour
        //---------------------------------------------

        //---------------------------------------------
        //Constructor
        //---------------------------------------------
        public Player(Level newLevel)
        {
            ourLevel = newLevel;
        }
        //---------------------------------------------

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }

        public void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>("player/player-stand");
        }

        public void Update(GameTime gameTime)
        {

            if(ourLevel.GetCompletedLevel() == true)
            {
                return;
            }

            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //Update velocity based on input, gravity, etc
            //Horizontal velocity is always constant, its an auto run game

            velocity.X = MOVE_SPEED;

            velocity.Y += 3400.0f * deltaTime;
            //Clammp our vertical velocity to a terminal range
            velocity.Y = MathHelper.Clamp(velocity.Y, -TERMINAL_VEL, TERMINAL_VEL);


            //Check if the player is jumping and change the velocity accordiongly
            Input(gameTime);

            // position 2 = position 1 + deltaPosition
            // deltaPosition = velosity * deltaTime
            prevPosition = position;
            position += velocity * deltaTime;

            //check for colision
            CheckTileCollision();
        }
        private void CheckTileCollision()
        {
            //Start off by assuming you are NOT touching the ground
            touchingGround = false;

            //use the player's bounding box to get a list of tiles that we are colliding width
            Rectangle playerBounds = GetBounds();
            Rectangle prevPlayerBounds = GetPrevBounds();
            //how to list the tiles?
            List<Tile> collidingTiles = ourLevel.GetTilesInBounds(playerBounds);

            //for each colliding tile, move ourselves out of the tile
            foreach (Tile collidingTile in collidingTiles)
            {
                //determine how far we are overlaping with the other tile
                Rectangle tileBounds = collidingTile.GetBounds();
                Vector2 depth = GetCollisionDepth(tileBounds, playerBounds);
                Tile.TileType tileType = collidingTile.GetTileType();

                //only resolve collision, when there is actually one
                //depth will be Vector2.Zero if there was no collision
                if (depth != Vector2.Zero)
                {
                    if (tileType == Tile.TileType.LETHAL)
                    {
                        //stop, yer dead
                        KillPlayer();
                    }

                    if (tileType == Tile.TileType.GOAL)
                    {
                        //stop, yer dead
                        ourLevel.CompleteLevel();
                    }

                    if(tileType == Tile.TileType.POINTS)
                    {
                        //add some points
                        score += collidingTile.GetPoints();

                        //hide the coin
                        collidingTile.Hide();
                        continue;
                    }

                    float absDepthX = Math.Abs(depth.X);
                    float absDepthY = Math.Abs(depth.Y);

                    //Resolve the collision along the shallow axis, as this is the one 
                    //we are the closest to the edge of and therefore easier to "squeeze out"
                    //Or you can think of it as we only just overlapped on that side

                    if (absDepthY < absDepthX)
                    {
                        bool fallingOntoTile = playerBounds.Bottom > tileBounds.Top && prevPlayerBounds.Bottom < -tileBounds.Top;

                        if (tileType == Tile.TileType.IMPASSIBLE || tileType == Tile.TileType.PLATFORM && fallingOntoTile)
                        {
                            //Y is our shallow axis
                            //Resolve the collision along the Y axis
                            position.Y += depth.Y;

                            playerBounds = GetBounds();

                            //only if our feet touch the ground
                            //should we assume that we are touching the ground
                            if (playerBounds.Bottom >= tileBounds.Top)
                            {
                                touchingGround = true;
                            }
                        }




                    }
                    //only handle left/right collisions if its this type of tile
                    else if (tileType == Tile.TileType.IMPASSIBLE)
                    {
                        //X is our shallow axis
                        //resolve the collision along the X axis
                        position.X += depth.X;

                        playerBounds = GetBounds();
                    }

                }
            }
        }

        private Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }

        private Rectangle GetPrevBounds()
        {
            return new Rectangle((int)prevPosition.X, (int)prevPosition.Y, sprite.Width, sprite.Height);
        }
        private Vector2 GetCollisionDepth(Rectangle tile, Rectangle player)
        {
            //this function calculates how far our rectangles are overlapping

            //Calculate the half sizes of both rectangles
            float halfWidthPlayer = player.Width / 2.0f;
            float halfHeightPlayer = player.Height / 2.0f;
            float halfWidthTile = tile.Width / 2.0f;
            float halfHeightTile = tile.Height / 2.0f;

            //calculate the centres of each rectangle
            Vector2 centrePlayer = new Vector2(player.Left + halfWidthPlayer,
                player.Top + halfHeightPlayer);

            Vector2 centreTile = new Vector2(tile.Left + halfWidthTile,
                tile.Top + halfHeightTile);

            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            //minimum distance these need to be to not collide/intersect
            float minDistanceX = halfWidthPlayer + halfWidthTile;
            float minDistanceY = halfHeightPlayer + halfHeightTile;

            //if we are not intersecting at all, return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
            {
                return Vector2.Zero;
            }

            //calculate and return the intersection depth
            //essentially, how much over the minimum intersection distance are we in each direction
            // to be short how much are they intersecting in that direction
            float depthX = 0;
            float depthY = 0;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;

            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }
        //---------------------------------------------
        private void Input(GameTime gameTime)
        {

            KeyboardState keystate = Keyboard.GetState();

            //allowed to jump?
            // only true when touching ground or we are holding the button 
            // and did not reach the max jump time

            bool allowetToJump = touchingGround == true ||
                (jumpLaunchInProgress == true && jumpButtonTime <= MAX_JUMP_TIME);

            if (keystate.IsKeyDown(Keys.Space) && touchingGround == true)
            {
                jumpLaunchInProgress = true;
                jumpButtonTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

                velocity.Y = JUMP_LAUNCH_VEL * (1.0f - (float)Math.Pow(jumpButtonTime / MAX_JUMP_TIME, JUMP_CONTROL_POWER));

                //scale launch velocity on how long the key is held
                //We should have max time for holding the button
            }

        }
        //---------------------------------------------
        public Vector2 GetPosition()
        {
            return position;
        }

        private void KillPlayer()
        {
            ourLevel.SetupLevel();
            position = Vector2.Zero;
            prevPosition = Vector2.Zero;
            jumpButtonTime = 0;
            jumpLaunchInProgress = false;
            score = 0;
            return;
        }

        public int GetScore()
        {
            return score;
        }
    }
}
